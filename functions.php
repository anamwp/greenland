<?php
/**
 * greenland functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package gl
 */

if ( ! function_exists( 'greenland_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

function greenland_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on greenland, use a find and replace
	 * to change 'greenland' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'greenland', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'greenland' ),
		'mobile' => esc_html__( 'Mobile', 'greenland' ),
		'footer-menu' => esc_html__( 'Footer Menu', 'greenland' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Woocommerce support
	 */

	add_theme_support( 'woocommerce' );
	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'greenland_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'greenland_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function greenland_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'greenland_content_width', 640 );
}
add_action( 'after_setup_theme', 'greenland_content_width', 0 );

/**
 * Load Custom Framework Options
 */
require_once get_template_directory() . '/inc/tx-framework.php';


/*
Register google font
*/
function greenland_google_fonts_url() {
		$font_url = '';

		$title_typo            = greenland_get_option( 'tx_title_font' );
		$header_typo           = greenland_get_option( 'tx_header_font' );
		$blockquote_typo       = greenland_get_option( 'tx_blockquote_font' );
		$paragraph_typo        = greenland_get_option( 'tx_paragraph_font' );
		$widget_typo           = greenland_get_option( 'tx_widget_font' );

		$font_url = add_query_arg(
			'family',
			urlencode(
				$title_typo['family'] .":". $title_typo['variant'] . "|" .
				$header_typo['family'] .":". $header_typo['variant'] . "|" .
				$blockquote_typo['family'] .":". $blockquote_typo['variant'] . "|" .
				$paragraph_typo['family'] .":". $paragraph_typo['variant'] . "|" .
				$widget_typo['family'] .":". $widget_typo['variant']
			),
			"//fonts.googleapis.com/css"
		);
		return $font_url;


}

function eventia_google_fonts_roboto_slab() {
	$font_urls = add_query_arg('family', urlencode("Roboto Slab"), "//fonts.googleapis.com/css");
	return $font_urls;
}


/**
 * Custom Css
 */
if(!function_exists('greenland_custom_css')){
    function greenland_custom_css(){
        $enable_boxed_layout = cs_get_option('boxed_width') ? cs_get_option('boxed_width') : '';
        $background_image_property = cs_get_option('background-image') ? cs_get_option('background-image') : '';

        $greenland_custom_color = cs_get_option('greenland_custom_color') ? cs_get_option('greenland_custom_color') : '';
        if($greenland_custom_color):

            $body_font_color = cs_get_option('body_font_color');
            $heading_font_color = cs_get_option('heading_font_color');

        endif;
        
            ?>
            <style type="text/css">
                <?php if($enable_boxed_layout): ?>
                body{
                    <?php if(greenland_array_get($background_image_property, 'color')): ?>
                    background-color:<?php echo $background_image_property['color']; ?> !important;
                    <?php endif;?>

                    <?php if (greenland_array_get($background_image_property, 'image')):?>
                    background-image:url(<?php echo esc_url($background_image_property['image']) ; ?>) !important;
                    <?php endif;?>

                    <?php if (greenland_array_get($background_image_property, 'repeat')):?>
                    background-repeat:<?php echo $background_image_property['repeat'] ; ?> !important;
                    <?php endif;?>

                    <?php if (greenland_array_get($background_image_property, 'size')):?>
                    background-size:<?php echo $background_image_property['size'] ; ?> !important;
                    <?php endif;?>

                    <?php if (greenland_array_get($background_image_property, 'attachment')):?>
                    background-attachment:<?php echo $background_image_property['attachment'] ; ?> !important;
                    <?php endif;?>

                    <?php if (greenland_array_get($background_image_property, 'position')):?>
                    background-position:<?php echo $background_image_property['position'] ; ?> !important;
                    <?php endif;?>
                        
                }
                <?php endif; ?>

                <?php if($greenland_custom_color):; ?>
                    
                    body, .entry-content{
                        
                        <?php if ($body_font_color):?>
                        color:<?php echo $body_font_color; ?> !important;
                        <?php endif;?>
                  
                    }

                    h1,h2, h3, h4{
                        <?php if ($heading_font_color):?>
                        color:<?php echo $heading_font_color; ?> !important;
                        <?php endif;?>
                    }
                <?php endif; ?>

            </style>
            <?php
        
    }
}




/**
 * Enqueue scripts and styles.
 */
function greenland_scripts() {
	wp_enqueue_style( 'greenland-google-fonts', greenland_google_fonts_url(), array(), '1.1' );
	wp_enqueue_style( 'greenland-roboto-slab', eventia_google_fonts_roboto_slab(), array(), '1.2' );

	wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/dist/css/bootstrap.css' );
    wp_enqueue_style('animatecss', get_template_directory_uri() . '/dist/css/animate.min.css' );
    wp_enqueue_style('font-awesome-css', get_template_directory_uri() . '/dist/css/font-awesome.min.css' );
    wp_enqueue_style('magnific-popup-css', get_template_directory_uri() . '/dist/css/magnific-popup.css' );
    wp_enqueue_style('owl-carousel-css', get_template_directory_uri() . '/dist/css/owl.carousel.css' );
	wp_enqueue_style('select-css', get_template_directory_uri() . '/dist/css/chosen.css' );

    wp_enqueue_style('greenland-css', get_template_directory_uri() . '/dist/css/greenland.css', array(), '1.1' );
	wp_add_inline_style('greenland-css', greenland_custom_css());

    wp_enqueue_script( 'greenland-navigation', get_template_directory_uri() . '/dist/js/navigation.js', array(), '20120206', true );

    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/dist/js/bootstrap.js', array('jquery'), '20120206', true );

	wp_enqueue_script('bootstrap-dropdownhover', get_template_directory_uri(). '/dist/js/bootstrap-dropdownhover.js', array('jquery'), '', true );

    wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/dist/js/owl.carousel.js', array('jquery'), '20120206', true );
	wp_enqueue_script('classie', get_template_directory_uri(). '/dist/js/classie.js', array('jquery'), '', true );

    wp_enqueue_script( 'magnific-popup-js', get_template_directory_uri() . '/dist/js/jquery.magnific-popup.js', array('jquery'), '20120206', true );


    wp_enqueue_script( 'greenland-skip-link-focus-fix', get_template_directory_uri() . '/dist/js/skip-link-focus-fix.js', array(), '20130115', true );

    wp_enqueue_script( 'greenland-js', get_template_directory_uri() . '/dist/js/greenland.js', array('jquery' ), '20130115', true );

    wp_enqueue_script( 'select-js', get_template_directory_uri() . '/dist/js/chosen.jquery.min.js', array('jquery' ), '20130115', true );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'greenland_scripts' );


/**
 * Sidebar script
 */
require get_template_directory() . '/inc/sidebar_scripts.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
/**
 * Load bootstrap nav walker
 */
require get_template_directory() . '/lib/wp_bootstrap_navwalker.php';




/*  Custom Style */

include get_template_directory() . '/inc/option_style.php';

include get_template_directory() . '/inc/helper_functions.php';


include get_template_directory() . '/inc/woocommerce-functions/hooks.php';

include get_template_directory() . '/inc/woocommerce-functions/helper-functions.php';

/* tgmpa */

require_once get_template_directory().'/lib/class-tgm-plugin-activation.php';

require_once get_template_directory().'/inc/plugin-activation.php';



