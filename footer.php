<?php
$tx_credit      = greenland_get_option( 'tx_themexpert_credit' ); //returns boolean value (true)
$copyright_text = greenland_get_option( 'tx_footer_copy' ); //returns text (string)
$footer_type    = greenland_get_option( 'tx_footer_type' );
$footer_page    = intval( greenland_get_option( 'tx_footer_page' ) );
?>


</div><!-- #content -->

<footer id="colophon" class="site-footer" >

<script type="text/javascript">
  (function() {
    window._pa = window._pa || {};
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/541957995000fc7f080000a0.js";
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
_pq.push(['track', 'wpdemo']);
  })();
</script>

	<?php
	if ( $footer_type === 'widget' ):
		$footer_option = greenland_get_option( 'tx_footer_column' );
		switch ( $footer_option ) {
			case 'col_1':
				get_template_part( 'template-parts/footer/footer', 'col_1' );
				break;

			case 'col_2_1':
				get_template_part( 'template-parts/footer/footer', 'cols_2_1' );
				break;

			case 'col_2_2':
				get_template_part( 'template-parts/footer/footer', 'cols_2_2' );
				break;

			case 'col_2_3':
				get_template_part( 'template-parts/footer/footer', 'cols_2_3' );
				break;

			case 'col_3_1':
				get_template_part( 'template-parts/footer/footer', 'cols_3_1' );
				break;

			case 'col_3_3':
				get_template_part( 'template-parts/footer/footer', 'cols_3_3' );
				break;

			case 'col_3_2':
				get_template_part( 'template-parts/footer/footer', 'cols_3_2' );
				break;

			case 'col_4':
				get_template_part( 'template-parts/footer/footer', 'cols_4' );
				break;

			default:
				get_template_part( 'template-parts/footer/footer', 'cols_4' );
		}
	endif;
	?>
	<section id="sub-footer">
		<div class="container">
			<div class="row footer-bootom">
				<div class="col-sm-6">
					<?php if(is_active_sidebar('footer-bottom')): ?>
						<div class="footer-menu">
							<?php dynamic_sidebar('footer-bottom'); ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-sm-offset-2 col-sm-4 ">

					<p class="gl-copy-right">
						<?php

						if ( isset( $copyright_text ) ) {
							printf( esc_html( $copyright_text ) );
						} else {
							printf( esc_html__( '&copy; 2016','greenland' ) );
						}

						?>
					</p>

				</div>
			</div>
		</div>

	</section> <!--End of sub-footer-->


	<div class="back-to-top">
		<button class="greenland-bt" title="<?php echo esc_attr__('Back to Top', 'greenland'); ?>"><i class="fa fa-angle-up"></i></button>
	</div>
</footer><!-- #colophon -->


</div><!-- #page -->
</div>
<!-- end of the /.tx-site-content-inner-->
</div>
<!-- end of the /.tx-site-content-->
</div>
<!-- end of the /.tx-site-pusher-->

<!--start tag in in the header variation in template-parts/head- v1/v2/.....-->
</div>
<!--end of the /.tx-site-container/#tx-site-container-->
<?php wp_footer(); ?>

<script type="text/javascript">
window._pq = window._pq || [];
_pq.push(['track', 'wpdemo'])
</script>

</body>
</html>
