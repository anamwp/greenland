<?php
/**
 * Template Name: coming soon
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


	<?php wp_head(); ?>,

</head>
<body>

<div id="primary" class="content-area">
	<div id="main" class="site-main">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );


					endwhile; // End of the loop.
					?>
				</div>


			</div>


		</div>


	</div><!-- #main -->
</div><!-- #primary -->










<?php wp_footer(); ?>

</body>
</html>