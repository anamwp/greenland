<?php
/*
 * Service post type single page template
 * */
get_header();
?>

	<main id="main" class="site-main">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="all-service">
						<h4><?php esc_html_e('All Service','greenland'); ?></h4>
					</div>
					<div class="service-cat">
						<?php
						$query = array(
							'post_type' => 'tx-service',
							'post_status' => 'publish'
						);
						$loop = new WP_Query($query);
						$current_id = (get_queried_object()!== null)? get_queried_object()->ID:'';
					

						echo "<ul class='service-post-title'>";
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
							<?php 	$tx_service_active =  ( $current_id == get_the_ID())? 'active':''; ?>
							<li class="<?php echo esc_attr($tx_service_active); ?>" ><a href="<?php the_permalink(); ?>" data-tx-service="<?php the_ID(); ?>"><?php the_title(); ?></a></li>
						<?php
						endwhile;
						echo "</ul>";
						wp_reset_query();

						?>
					</div>
					<?php  if(is_active_sidebar('service')){
						if(dynamic_sidebar('service'));
					}?>

				</div>
				<div class="col-md-8">
					<?php
					while ( have_posts() ) : the_post();


						get_template_part( 'template-parts/content', 'service' );


					endwhile; // End of the loop.
					?>

					<div class="related_post">

							<?php  $current_service_id = get_the_ID();
							echo do_shortcode("[tx-service orderby='rand' item='2' not_post= esc_html($current_service_id)  column_num = '6' ]"); ?>


					</div><!--end of related post-->
				</div>

			</div>
		</div>


	</main><!-- #main -->


<?php
get_footer();
?>
