<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package _x
 */

get_header(); ?>

	<div id="primary" class="content-area page-404">
		
		<div id="main" class="site-main">
			<div class="container">
				<section class="error-404">
					<h1 class="page-title"><?php esc_html_e( '404', 'greenland' ); ?></h1>
					<p class="error-meesage"> <?php esc_html_e('Something Went Wrong ', 'greenland') ?></p>

					<div class="page-content">
						<h5><?php esc_html_e( 'Sorry, the page could not be found', 'greenland' ); ?></h5>
						<p><?php esc_html_e( 'Cant find what you need? Take a moment and do a search below or start from our homepage.', 'greenland' ); ?></p>

						<?php
						get_search_form();

						?>
						<a class="go-home" href="<?php echo esc_url( home_url( '/' ) ); ?>"> <?php esc_html_e('Go Home', 'greenland'); ?>  </a>

					</div><!-- .page-content -->
				</section><!-- .error-404 -->
			</div>


		</div><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
