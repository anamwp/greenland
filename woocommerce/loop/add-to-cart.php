<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see        http://docs.woothemes.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $post;
global $product;
$parentid = $product->id;

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );




// check for plugin using plugin name
$compare_button = '';
$wishlist_button = '';
if ( is_plugin_active( 'yith-woocommerce-compare/init.php' ) ) {
	//plugin is activated
	$compare_button = "[yith_compare_button product=$parentid]";
}

if ( is_plugin_active( 'yith-woocommerce-wishlist/init.php' ) ) {
	//plugin is activated
	$wishlist_button = '[yith_wcwl_add_to_wishlist icon="fa fa-heart fa-fw"]';
}
?>
<?php if(is_product()||is_shop() ): ?>
<div class="quick-cart">

	<?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
		sprintf( '<div class="shop-attribute"><div class="compare-link">%s</div><div class="shop-cart"><a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a></div><div class="shop-wishlist">%s</div></div>',
			do_shortcode( "$compare_button" ),
			esc_url( $product->add_to_cart_url() ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			esc_attr( $product->id ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $class ) ? $class : 'button' ),
			$product->add_to_cart_text(),
			str_replace( '<div class="clear"></div>', '', do_shortcode( $wishlist_button ) )

		),
		$product, 20
	);
	?>
</div>
	<?php else:
	echo apply_filters( 'woocommerce_loop_add_to_cart_link',
	sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
	esc_url( $product->add_to_cart_url() ),
	esc_attr( isset( $quantity ) ? $quantity : 1 ),
	esc_attr( $product->id ),
	esc_attr( $product->get_sku() ),
	esc_attr( isset( $class ) ? $class : 'button' ),
	esc_html( $product->add_to_cart_text() )
	),
	$product );

 endif; ?>










