<?php
/**
 * Share
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.10
 */

?>
<div class="gl-woo-share">
	<?php if ( shortcode_exists( 'greenland-share' ) ) : ?>
		<span><?php esc_html_e('Share:', 'greenland') ?> </span>
		<?php echo do_shortcode("[greenland-share]"); ?>
	<?php endif;  ?>
</div>

