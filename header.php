<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _x
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php

	$default_header = greenland_get_option('tx_header_select') ? greenland_get_option('tx_header_select') : '1' ;
	$gl_page_id      = greenland_get_all_pages_ID(); //get page id
	$selected_header = get_post_meta( $gl_page_id, '_tx_greenload_header_select', true );
	if(!empty($selected_header) && !empty($selected_header['header_bg-select'])):
	$selected_header = $selected_header['header_bg-select'];
	else:
		$selected_header = $default_header;
	endif;
	?>
	<?php wp_head(); ?>
	
	<script type="text/javascript">
	  (function() {
	    window._pa = window._pa || {};
	    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
	    pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//tag.marinsm.com/serve/541957995000fc7f080000a0.js";
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);
	  })();
	</script>

</head>

<?php echo  get_template_part( 'template-parts/header/header',esc_html($selected_header) ); ?>