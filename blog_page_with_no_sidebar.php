<?php
/**
 * Template Name: Blog page with no sidebar
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row">
					<div class="col-md-12 gl-1-column">
						<?php
						if (is_front_page()) {
							$page = get_query_var('page');
							$paged = ($page) ? $page : 1;
						} else {
							$page = get_query_var('paged');
							$paged = ($page) ? $page : 1;
						}

						$args = array(
							'post_type' => 'post',
							'posts_per_page' => get_option('posts_per_page'),
							'paged' => $paged,
						);
						$wp_query = new WP_Query($args);
						?>
						<?php
						if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_format() );

						endwhile;

						greenland_pagination();

						else :

						get_template_part( 'template-parts/content', 'none' );

						endif;
						wp_reset_query();
						?>
					</div>

				</div>
			</div>


		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer(); ?>