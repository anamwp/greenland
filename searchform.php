<?php
/**
 * Custom Search form
 *
 * Greenland theme
 */
?>
<form method="get" class="search-form custom-search" action="<?php echo esc_url(home_url( '/' )); ?>">


		<input type="search" class="search-field " placeholder="<?php esc_attr_e('Search', 'greenland') ?>" name="s" title="<?php esc_attr_e('Search for:', 'greenland'); ?>" />


		<button type="submit" class="fa fa-search search-submit">	</button>
</form>

