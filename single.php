<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _x
 */

get_header();
$sidebar_position = greenland_get_option('tx_sidebar_position') ? greenland_get_option('tx_sidebar_position') : 'right' ;
?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<div class="row">

					<?php if( $sidebar_position == 'left' ):?>
						     <div class="col-md-4">
							     <?php  get_sidebar(); ?>
						     </div>

					<?php endif; ?>
						<?php if( $sidebar_position == 'none' ):?>
								<div class="col-md-12">
							<?php else: ?>
									<div class="col-md-8">
						<?php endif; ?>
						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', get_post_format() );
							?>
							<div class="blog-author-infor">
								<?php greenland_author_content_hook(); ?>
							</div>
							<div class="col-sm-12 tx-single-blog-share">
								<div class="col-sm-6">
									<h5><?php esc_html_e('Did You Like This Post? Share It:', 'greenland'); ?></h5>
								</div>
								<div class="col-sm-6">

									<?php if ( shortcode_exists( 'greenland-share' ) ) : ?>
										<?php echo do_shortcode("[greenland-share]"); ?>
									<?php endif;  ?>
								</div>

							</div><!--end of tx-single-share-->
						

							<?php


							greenland_custom_nav();

							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

						endwhile; // End of the loop.
						?>
					</div>
					<?php if( $sidebar_position == 'right' ):?>
						<div class="col-md-4">
							<?php  get_sidebar(); ?>
						</div>

					<?php endif; ?>
				</div>
			</div>
		

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
