<?php

/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package _x
 */

get_header();
$sidebar_position = greenland_get_option('tx_sidebar_position') ? greenland_get_option('tx_sidebar_position') : 'right' ;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


			<div class="container">
				<div class="row">

					<?php if( $sidebar_position == 'left' ):?>
						<div class="col-md-4 sidebar-left">
							<?php  get_sidebar(); ?>
						</div>
					<?php endif; ?>
<!--					end of /.sidebar-left-->

					<?php if( $sidebar_position == 'none' ):?>
					<div class="col-md-12">
					<?php else: ?>
					<div class="col-md-8 blog-listing">
					<?php endif; ?>
						<?php
						if ( have_posts() ) :
							/* Start the Loop */
							while ( have_posts() ) : the_post();
								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/content', get_post_format() );
							endwhile;
							greenland_pagination();
						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>

					</div>
<!--						end of /.col-md-8 or /.col-md-12 -->


					<?php if( $sidebar_position == 'right' ):?>
						<div class="col-md-4 sidebar-right">
							<?php  get_sidebar(); ?>
						</div>
					<?php endif; ?>

				</div>
<!--					end of /.row-->
			</div>
<!--				end of /.container-->


		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
