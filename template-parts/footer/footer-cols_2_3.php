    <section id="footer">
        <?php if(is_active_sidebar('footer-1')||is_active_sidebar('footer-2')): ?>
            <div class="container">
                <div class="row">
                    <div class="footer-1">
                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12 ">
                            <?php if(dynamic_sidebar('footer-1')); ?>
                        </div>
                    </div> <!--end of footer-1-->

                    <!--start footer-2 widget-->
                    <div class="footer-2">
                        <div class="col-lg-4 col-md-4 col-sm-7 col-xs-12">
                            <?php if(dynamic_sidebar('footer-2')); ?>
                        </div>
                    </div> <!--end of footer-2-->

                </div>   <!--end of row-->
            </div>  <!--end of container-->
        <?php endif; ?>
    </section> <!-- end of footer section-->

