    <section id="footer">
        <?php if(is_active_sidebar('footer-1')): ?>
            <div class="container">
                <div class="row">
                    <div class="footer-1">
                        <div class="col-lg-12 center-widget">
                            <?php if(dynamic_sidebar('footer-1')); ?>
                        </div>
                    </div> <!--end of footer-1-->
                </div>   <!--end of row-->
            </div>  <!--end of container-->
        <?php endif; ?>
    </section> <!-- end of footer section-->

 