	<section id="footer">
		<?php if(is_active_sidebar('footer-1')||is_active_sidebar('footer-2')||is_active_sidebar('footer-3')): ?>
			<div class="container">
				<div class="row">
					<div class="footer-1">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  center-widget">
							<?php if(dynamic_sidebar('footer-1')); ?>
						</div>
					</div> <!--end of footer-1-->

					<!--start footer-2 widget-->
					<div class="footer-2">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<?php if(dynamic_sidebar('footer-2')); ?>
						</div>
					</div> <!--end of footer-2-->

					<!-- Start footer-3 Widget-->
					<div class="footer-3">
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<?php if(dynamic_sidebar('footer-3')); ?>
						</div>
					</div> <!--end of footer-3-->

				</div>   <!--end of row-->
			</div>  <!--end of container-->
		<?php endif; ?>
	</section> <!-- end of footer section-->
