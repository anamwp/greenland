<?php
/**
 * Created by PhpStorm.
 * User: apurba
 * Date: 5/13/2016
 * Time: 4:48 PM
 */

?>


<div class="slide-content-wrapper" >
	<div class="slide-content">

		<div class="slide-header">
			<h4 class="cart-total">

				<a
					class="cart-contents"
				   	href="<?php echo WC()->cart->get_cart_url(); ?>"
					title="<?php esc_attr_e('View your shopping cart', 'greenland'); ?>"
				>
					<?php echo sprintf (_n( '%d item', '%d items', 'greenland'), WC()->cart->get_cart_contents_count() ); ?>
					-
					<?php echo WC()->cart->get_cart_total(); ?>
				</a>
				


				<span><?php esc_html_e('in cart', 'greenland'); ?></span>
			</h4>
			<button type="button" class="close" ><span aria-hidden="true">&times;</span></button>
		</div>

		<div class="slide-body">

			<?php the_widget('WC_Widget_Cart') ?>

		</div>
		<!-- end of the modal body -->



		<div class="slide-footer">

		</div>


	</div>
</div>

