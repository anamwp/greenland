<?php
/**
 *blog-list content template
 */
$blog_text_position = greenland_get_option('tx_blog_position');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( is_single() ): ?>
		<div class="tx-single-post">
			<?php if ( is_sticky() ): ?>
				<?php if(has_post_thumbnail()): ?>
					<div class="sticky-post">
						<div class="sticky-post-wrapper">
							<i class="fa fa-star"></i>
						</div>
						<div class="triangle"></div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<?php if ( is_sticky() ): ?>
				
			<?php endif; ?>
			<?php  if ( has_post_thumbnail() ):?>
				<div class="post-image-wrapper">
					<div class="post-image ">
						<?php the_post_thumbnail( 'full', array( 'class' => 'full-width-image' ) ); ?>
					</div>
				</div><!--end of post-image-->
			<?php endif; ?>

			<div class="post-excrept">
				<div class="post-time">
					<?php if ( is_sticky() ): ?>
						<?php if(! has_post_thumbnail()): ?>
							<div class="sticky-post-no-thumbnail">
								<div class="sticky-post-wrapper">
									<i class="fa fa-star"></i>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
					<div class="inner-div">
						<h3 class="gl-title gl-post-time"><?php  echo greenland_post_time(); ?></h3>
					</div>

				</div>
				<div class="post-title">
					<?php

					the_title( '<h3 class="entry-title ">', '</h3>' );

					?>
				</div><!--end of post-title-->
				<div class="post-meta">
					<div class="inner-div">
						<?php greenland_posted_on(); ?>
					</div>
				</div><!--end of post-meta-->

				<div class="post-content">
					<?php
					the_content();
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'greenland' ),
						'after'  => '</div>',
					) );
					?>
				</div><!--end of post-content-->



			</div><!--end of post-excerpt -->


		</div><!--archrive single post full-->
	<?php else: ?>
		<div class="tx-single-post-row greenland-content">
			<?php if ( is_sticky() ): ?>
				<?php if(has_post_thumbnail()): ?>
					<div class="sticky-post">
						<div class="sticky-post-wrapper">
							<i class="fa fa-star"></i>
						</div>
						<div class="triangle"></div>
					</div>
				<?php endif; ?>
			<?php endif; ?>
			<?php  if ( has_post_thumbnail() ):?>
				<div class="post-image-wrapper">
				<div class="post-image ">
					<?php the_post_thumbnail( 'full', array( 'class' => 'full-width-image' ) ); ?>
				</div><!--end of post-image-->
				</div>
			<?php endif; ?>

			<div class="post-excrept">
				<div class="post-time">

					<?php if ( is_sticky() ): ?>
						<?php if(! has_post_thumbnail()): ?>
							<div class="sticky-post-no-thumbnail">
								<div class="sticky-post-wrapper">
									<i class="fa fa-star"></i>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>

					<div class="inner-div">
						<h3 class="gl-title gl-post-time"><?php  echo greenland_post_time(); ?></h3>
					</div>

				</div>

				<div class="post-title">
					<?php
					the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

					if ( 'post' === get_post_type() ) {
						/* translators: used between list items, there is a space after the comma */
						$categories_list = get_the_category_list( esc_html__( ', ', 'greenland' ) );
						if ( $categories_list && greenland_categorized_blog() ) {
							printf( '<span class="cat-links">' . esc_html__( 'Cat: %1$s', 'greenland' ) . '</span>', $categories_list ); // WPCS: XSS OK.
						}
					}

					?>

				</div><!--end of post-title-->
				<div class="post-meta">
					<?php greenland_posted_on(); ?>
				</div><!--end of post-meta-->

				<div class="post-content text-<?php esc_html_e($blog_text_position, 'greenland'); ?>">
					<?php
					the_excerpt();
					wp_link_pages( array(
						'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'greenland' ),
						'after'  => '</div>',
					) );
					?>
				</div><!--end of post-content-->
				<div class="post-link text-<?php esc_html_e($blog_text_position, 'greenland'); ?>">
					<a href="<?php the_permalink(); ?>"><?php esc_html_e('Read More...', 'greenland'); ?></a>

				</div><!--end of post-link-->


			</div><!--end of post-excerpt -->

		</div><!--archrive single post full-->
	<?php endif; ?>

</article>

