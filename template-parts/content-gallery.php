<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package arsim
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ( is_single() ): ?>
        <div class="tx-single-post-sp greenland-content-galley-single">
            <?php if ( is_sticky() ): ?>
                <?php if(has_post_thumbnail()): ?>
					<div class="sticky-post">
						<div class="sticky-post-wrapper">
							<i class="fa fa-star"></i>
						</div>
						<div class="triangle"></div>
					</div>
				<?php endif; ?>
            <?php endif; ?>


            <div class="post-excrept">
                <div class="post-time">
                    <div class="inner-div">
                        <h3 class="gl-title gl-post-time"><?php  echo greenland_post_time(); ?></h3>
                    </div>

                </div>
                <div class="post-title">
                    <?php

                    the_title( '<h3 class="entry-title ">', '</h3>' );

                    ?>
                </div><!--end of post-title-->
                <div class="post-meta">
                    <div class="inner-div">
                        <?php greenland_posted_on(); ?>
                    </div>
                </div><!--end of post-meta-->

            </div><!--end of post-excerpt -->
            <div class="post-content">
                <?php the_content(); ?>
            </div><!--end of post-content-->
        </div><!--archrive single post full-->
        
    <?php else: ?>
        <div class="tx-single-post-row greenland-content-gallery">
            <?php if ( is_sticky() ): ?>
                <?php if(has_post_thumbnail()): ?>
					<div class="sticky-post">
						<div class="sticky-post-wrapper">
							<i class="fa fa-star"></i>
						</div>
						<div class="triangle"></div>
					</div>
				<?php endif; ?>
            <?php endif; ?>


            <div class="post-image">
                <div class="post-content">
                    <?php the_content(); ?>
                </div><!--end of post-content-->
            </div><!--end of post-image-->

            <div class="post-excrept">
                <div class="post-time">
                    <div class="inner-div">
                        <h3 class="gl-title gl-post-time"><?php  echo greenland_post_time(); ?></h3>
                    </div>

                </div>
                <div class="post-title">
                    <?php
                    the_title( '<h3 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );

                    ?>
                </div><!--end of post-title-->
                <div class="post-meta">
                    <?php greenland_posted_on(); ?>

                </div><!--end of post-meta-->
                <div class="post-link">
                    <a href="<?php the_permalink(); ?>"><?php esc_html_e('Read More...', 'greenland'); ?></a>

                </div><!--end of post-link-->
            </div><!--end of post-excerpt -->



        </div><!--archrive single post full-->
    <?php endif; ?>

</article>

