<?php
/**
 * Created by PhpStorm.
 * User: apurba
 * Date: 6/14/2016
 * Time: 9:23 AM
 */
?>
<?php
$tx_service_gallery =  get_post_meta( $post->ID, '_tx_service_gallery', true );
$tx_service_gallery = explode(",",$tx_service_gallery['_tx_gallery_img']);
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if ( is_single() ): ?>
	<div class="service-single">
		<div class="post-excrept">
			<?php if(has_post_thumbnail()): ?>
				<div class="service-featured-image">
						<?php the_post_thumbnail('full'); ?>
				</div>
			<?php endif; ?>

			<div class="service-title">
				<?php
					the_title( '<h3 class="entry-title ">', '</h3>' );
				?>
			</div><!--end of post-title-->

			<div class="post-content">
				<?php the_content(); ?>
			</div><!--end of post-content-->

		</div><!--end of post-excerpt -->

		<?php if(strlen($tx_service_gallery[0])>0): ?>
			<div class="gl-service-gallery">
				<div class="owl-carousel service-gallery">
					<?php foreach($tx_service_gallery as $image): ?>
						<div>
							<div class="gallery-img">
								<img src="<?php echo wp_get_attachment_image_src(  esc_html($image), 'full' )[0]; ?>"
									 alt=" Service gallery">
							</div>
						</div>

					<?php endforeach; ?>

				</div><!--end of gallery-->
			</div><!--end of gl-service-gallery-->
		<?php endif; ?>

	</div><!--archrive single post full-->



<?php endif; ?>

</article>