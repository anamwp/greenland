<?php
/*
 * Header template 1
 * */
/**
 * Getting Option values for Header
 */


$logo             = greenland_get_option( 'tx_logo_header_1' ); //returns wp image attachment id
$header_variation = greenland_get_option( 'tx_header_select' ); //returns text (string)
$enable_boxed_layout = cs_get_option('boxed_width');

$show_social     = greenland_get_option( 'tx_social_icons' ); //returns boolean value (true)
$social_links    = greenland_get_option( 'tx_top_social' ); //returns 2D Array
$social_fb       = $social_links['tx_top_social_fb']; //returns Social Link (facebook)
$social_tw       = $social_links['tx_top_social_tw']; //returns Social Link (twitter)
$social_yt       = $social_links['tx_top_social_yt']; //returns Social Link (youtube)
$social_ln       = $social_links['tx_top_social_ln']; //returns Social Link (linkedin)
$social_sk       = $social_links['tx_top_social_sk']; //returns Social Link (Skype)
$gl_top_bar      = greenland_get_option( 'tx_top_bar' );//returns boolean value (true)
$top_bar_address = greenland_get_option( 'tx_top_address' );//returns 2D Array
$top_address     = $top_bar_address['tx_top_address'];
$top_contact     = $top_bar_address['tx_top_cell_no'];
$top_email       = $top_bar_address['tx_top_email'];
$gl_page_id      = greenland_get_all_pages_ID(); //get page id
$gl_extra_button = greenland_get_option('tx_extra_button');/*Header 1 button switch*/
$gl_eb_pops      = greenland_get_option('tx_extra_button_properties');

?>

<body <?php body_class(); ?>>

<!-- New Coded start Mobile Menu Option-->
<div class="tx-site-container <?php echo $enable_boxed_layout ? ' boxed-width' : ''; ?>" id="tx-site-container">

	<!--   start mobile menu -->
	<nav class="tx-menu tx-effect-1" id="menu-1">
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'mobile',
				'menu'			=> 'mobile',
			)
		);
		?>
	</nav>
	<!--    end mobile menu -->
	<div class="tx-site-pusher">
		<div class="tx-site-content">
			<div class="tx-site-content-inner">

<!-- end Mobile Menu Option-->




<div id="page" class="site">

	<header id="masthead" class="site-header">
		<?php if ( $gl_top_bar ): ?>
			<div class="top-header hidden-xs">
				<div class="container">
					<div class="row th-row header-1-row">
						<div class="col-sm-8">
							<ul class="address">
								<?php if ( $top_address ): ?>
									<li><i class="fa fa-home"></i> <?php echo esc_html($top_address); ?></li>
								<?php endif; ?>
								<?php if ( $top_contact ): ?>
									<li><i class="fa fa-phone"></i><?php echo esc_html($top_contact); ?></li>
								<?php endif; ?>
								<?php if ( $top_email ): ?>
									<li><i class="fa fa-envelope-o"></i> <?php echo esc_html($top_email); ?>
									</li>
								<?php endif; ?>
							</ul>
						</div>
<!--						end of /.col-sm-8-->
						<div class="col-sm-4">
							<?php if ( ! empty( $social_links ) ): ?>
							<ul class="Header-social social-link">
								<?php if ( $social_tw ): ?>
									<li>
										<a target="_blank" href="<?php echo esc_url($social_tw); ?>">
											<i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
										</a>
									</li>
								<?php endif; ?>

								<?php if ( $social_ln ): ?>
									<li><a target="_blank" href="<?php echo esc_url($social_ln); ?>"><i class="fa fa-linkedin fa-fw" aria-hidden="true"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( $social_fb ): ?>
									<li><a target="_blank" href="<?php echo esc_url($social_fb); ?>"><i class="fa fa-facebook fa-fw" aria-hidden="true"></i></a>
									</li>
								<?php endif; ?>

								<?php if ( $social_sk ): ?>
									<li><a target="_blank" href="<?php echo esc_url($social_sk); ?>"><i class="fa fa-skype fa-fw" aria-hidden="true"></i></a></li>
								<?php endif; ?>

								<?php if ( $social_yt ): ?>
									<li><a target="_blank" href="<?php echo esc_url($social_yt); ?>"><i class="fa fa-youtube fa-fw" aria-hidden="true"></i></a></li>
								<?php endif; ?>

							</ul>
							<?php endif; ?>
						</div>
<!--						end of /.col-sm-4 -->
					</div>
<!--					end of /.th-row-->
				</div>
				<!--	end of container-->
			</div>
			<!--end of top header-->
		<?php endif; ?>
		<div class="search-bar logo-search-cart">
			<div class="container">
				<div class="row search-row">
					<div class="col-sm-4 hidden-xs">
						<?php get_search_form(); ?>
					</div>
					<div class="col-sm-4 gl-logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<?php
							/**
							 * Output Site Logo
							 */

							if (  $logo  ) {
								echo wp_get_attachment_image( esc_html($logo), 'full' );
							} else {
								?>
								<h3>
									<?php
										$blog_name = get_bloginfo('name');
										if($blog_name){
											esc_html_e($blog_name, 'greenland');
										}else{
											esc_html_e('Greenland', 'greenland');
										}
									?>
								</h3>
							<?php
							}
							?>
						</a>
					</div><!--end of gl-logo-->
					<div class="col-sm-4  hidden-xs">
						<div class="pull-right">
							<?php do_action( 'gland_cart' ); ?>
						</div>
					</div><!--end col-sm-4-->
					<div class="gl-woo-header-cart">
						<div id="gl-woo-cart-modal">
							<?php
							if ( class_exists( 'WooCommerce' ) ):
								get_template_part( 'template-parts/woo-cart-template', 'modal' );
							endif;
							?>
						</div> <!--gl-woo-cart-modal-->
					</div>
					<div class="greenland-res-menu-icon-wrapper greenland-visible-xs-768 col-xs-12">
						<div id="tx-trigger-effects">
							<button data-effect="tx-effect-1"><i class="fa fa-bars"></i></button>
						</div>
					</div>
				</div>
<!--				end of /.search-row -->
			</div>
<!--			end of /.container -->
		</div>
<!--		end of /.search-bar-->

		<div class="greenland-main-menu greenland-header-1">
			<div class="container gl-toggle-menu" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">
					<div class="col-lg-9 col-md-9 col-sm-12 main-menu-left-side-wrapper">
						<nav id="greenland-mainnav" class="navbar navbar-default greenland-mainnav">
							<div class="">
								<?php
								wp_nav_menu(
									array(
										'theme_location' => 'primary',
										'menu'			=> 'primary',
										'menu_class'	=> 'nav navbar-nav',
										'depth'			=> 4,
										'container'		=> 'div',
										'container_class'	=> 'navbar-collapse collapse greenland-nav',
										'container_id'		=> 'greenland-main-nav',
										'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
										'walker'			=> new wp_bootstrap_navwalker()
									)
								);
								?>
							</div>
							<!-- end of the nav/.container -->
						</nav><!-- #site-navigation -->
					</div>
					<!--                    end of the /.main-menu-left-side-wrapper-->

					<?php if ( $gl_extra_button ): ?>
					<div class="col-md-3 right-side-button main-menu-right-side-wrapper pull-right">
						<ul class="nav navbar-nav navbar-right ex-btn">
							<li>
								<?php if($gl_eb_pops['tx_1st_button_text']):?>
								<a href="<?php echo esc_url_raw($gl_eb_pops['tx_1st_button_url']); ?>" class="gl-contact-us gl-first-c-us">
									<i class="fa <?php  echo esc_html($gl_eb_pops['tx_1st_button_icon'])  ; ?>" aria-hidden="true"></i>
									<?php echo esc_html( $gl_eb_pops['tx_1st_button_text'] ) ?>
								</a>
								<?php endif; ?>
							</li>
							<li>
								<?php if($gl_eb_pops['tx_2nd_button_text']):?>
								<a href="<?php echo esc_url_raw($gl_eb_pops['tx_2nd_button_url']); ?>" class="gl-contact-us">
									<i class="fa <?php  echo esc_html($gl_eb_pops['tx_2nd_button_icon'])  ; ?>" aria-hidden="true"></i>
									<?php echo esc_html( $gl_eb_pops['tx_2nd_button_text'] ) ?>
								</a>
								<?php endif; ?>
							</li>

						</ul>
					</div><!--end of right-side-button-->

				<?php endif; ?>
			</div>
		</div><!-- #site-navigation -->
	</header><!-- #masthead -->


	<?php
	$gl_subtitle = '';
	$gl_subtitle_meta     = get_post_meta($gl_page_id,'_tx_greenload_post_subtitle',true);
	if ( ! empty( $gl_subtitle_meta ) ){
		$gl_subtitle = $gl_subtitle_meta['greenland-subtitle'];
	}
	$gl_header_bg                = '';
	$gl_header_bg                = get_post_meta( $gl_page_id, '_tx_greenload_header_image', true );
	$gl_header_bg_properties     = $gl_header_bg_height = $gl_header_bg_switch = '';
	if ( ! empty( $gl_header_bg ) ):
		$gl_header_bg_switch = ( isset( $gl_header_bg['header_bg-switch'] ) ) ? $gl_header_bg['header_bg-switch'] : '';
		$gl_header_bg_properties = $gl_header_bg['_tx_header_bg'];



		?>

		<?php if ( $gl_header_bg_switch ): ?>
		<div class="header_bg greenland-breadcrumb"
			 style="background: url(<?php echo esc_url($gl_header_bg_properties['image']); ?>);
				 background-color: <?php echo esc_attr($gl_header_bg_properties['color']); ?>;
				 background-size: <?php echo esc_attr( $gl_header_bg_properties['size']); ?>;
				 background-position: <?php echo esc_attr( $gl_header_bg_properties['position']); ?>;
				 background-repeat: <?php echo esc_attr( $gl_header_bg_properties['repeat']); ?>;
				 background-attachment: <?php echo esc_attr( $gl_header_bg_properties['attachment']); ?>;">
			<div class="gl-title-txt greenland-breadcrumb-wrapper">
				<h1 class="greenland-title" ><?php echo wp_title(''); ?></h1>
				<h3 class="greenland-breadcrumb-link"><?php woocommerce_breadcrumb(); ?></h3>
			</div>

		</div>
	<?php endif; ?>
	<?php endif; ?>
	<div id="content" class="site-content">
