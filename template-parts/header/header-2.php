<?php
   /*
    * Header template 1
    * */
   /**
    * Getting Option values for Header
    */


$logo             = greenland_get_option( 'tx_logo_header_2' ); //returns wp image attachment id
$header_variation = greenland_get_option( 'tx_header_select' ); //returns text (string)
$enable_boxed_layout = cs_get_option('boxed_width');

$show_social     = greenland_get_option( 'tx_social_icons' ); //returns boolean value (true)
$social_links    = greenland_get_option( 'tx_top_social' ); //returns 2D Array
$social_fb       = $social_links['tx_top_social_fb']; //returns Social Link (facebook)
$social_tw       = $social_links['tx_top_social_tw']; //returns Social Link (twitter)
$social_yt       = $social_links['tx_top_social_yt']; //returns Social Link (youtube)
$social_ln       = $social_links['tx_top_social_ln']; //returns Social Link (linkedin)
$social_sk       = $social_links['tx_top_social_sk']; //returns Social Link (Skype)
$gl_top_bar      = greenland_get_option( 'tx_top_bar' );//returns boolean value (true)
$top_bar_address = greenland_get_option( 'tx_top_address' );//returns 2D Array
$top_address     = $top_bar_address['tx_top_address'];
$top_contact     = $top_bar_address['tx_top_cell_no'];
$top_email       = $top_bar_address['tx_top_email'];
$gl_page_id      = greenland_get_all_pages_ID(); //get page id
$gl_extra_button = greenland_get_option('tx_extra_button');/*Header 1 button switch*/
$gl_eb_pops      = greenland_get_option('tx_extra_button_properties');

?>
<body <?php body_class(); ?>>

<!-- New Coded start Mobile Menu Option-->
<div class="tx-site-container <?php echo $enable_boxed_layout ? ' boxed-width' : ''; ?>" id="tx-site-container">

	<!--   start mobile menu -->
	<nav class="tx-menu tx-effect-1" id="menu-1">
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'mobile',
				'menu'			=> 'mobile',
			)
		);
		?>
	</nav>
	<!--    end mobile menu -->
	<div class="tx-site-pusher">
		<div class="tx-site-content">
			<div class="tx-site-content-inner">

<!-- end Mobile Menu Option-->


   <div id="page" class="site">
	   <header id="masthead" class="site-header">
		  <!-- #site-navigation -->
		  <div class="header-2-menu greenland-main-menu greenland-header-2">
			 <div class="container h2-menu ">
				<div class="col-md-2 col-sm-2 logo">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<?php
						/**
						 * Output Site Logo
						 */
						if ( isset( $logo ) ) {
							echo wp_get_attachment_image( esc_html($logo), 'full' );
						} else {
							?>
							<h3>
								<?php
									$blog_name = get_bloginfo('name');
									if($blog_name){
										esc_html_e($blog_name, 'greenland');
									}else{
										esc_html_e('Greenland', 'greenland');
									}
								?>
							</h3>
							<?php
						}
						?>
					</a>
				</div>
	<!--			 end of /.logo-->
				<div class="col-md-9 col-sm-10 gl-h2-menu" data-hover="dropdown" data-animations="zoomIn zoomIn zoomIn zoomIn">
					<nav id="greenland-mainnav" class="navbar navbar-default greenland-mainnav">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'primary',
									'menu'			=> 'primary',
									'menu_class'	=> 'nav navbar-nav',
									'depth'			=> 4,
									'container'		=> 'div',
									'container_class'	=> 'navbar-collapse collapse ',
									'container_id'		=> 'greenland-main-nav',
									'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
									'walker'			=> new wp_bootstrap_navwalker()
								)
							);
						?>
					</nav>
				</div>
	<!--			 end of the /.greenland-h2-menu-->
				<div class="col-md-1 main-menu-right-side-wrapper padding-top hidden-sm hidden-xs ">
				   <div class="col-xs-6">
					  <?php get_search_form(); ?>
				   </div>
				   <div class="col-xs-6">
					   <?php do_action( 'gland_cart2' ); ?>
				   </div>
				</div>
	<!--			 end of the /.right-section -->
				 <div class="greenland-res-menu-icon-wrapper greenland-visible-xs-768">
					 <div id="tx-trigger-effects">
						 <button data-effect="tx-effect-1"><i class="fa fa-bars"></i></button>
					 </div>
				 </div>
	<!--			 end of the mobile view button-->
			 </div>
		  </div>
	   </header>
	   <?php
	   $gl_subtitle = '';
	   $gl_subtitle_meta     = get_post_meta($gl_page_id,'_tx_greenload_post_subtitle',true);
	   if ( ! empty( $gl_subtitle_meta ) ){
		   $gl_subtitle = $gl_subtitle_meta['greenland-subtitle'];
	   }
	   $gl_header_bg                = '';
	   $gl_header_bg                = get_post_meta( $gl_page_id, '_tx_greenload_header_image', true );
	   $gl_header_bg_properties     = $gl_header_bg_height = $gl_header_bg_switch = '';
	   if ( ! empty( $gl_header_bg ) ):
		   $gl_header_bg_switch = ( isset( $gl_header_bg['header_bg-switch'] ) ) ? $gl_header_bg['header_bg-switch'] : '';
		   $gl_header_bg_properties = $gl_header_bg['_tx_header_bg'];
		   ?>

		   <?php if ( $gl_header_bg_switch ): ?>
			   <div class="header_bg"
					style="background: url(<?php echo esc_url($gl_header_bg_properties['image']); ?>);
						background-color: <?php echo esc_attr($gl_header_bg_properties['color']); ?>;
						background-size: <?php echo esc_attr( $gl_header_bg_properties['size']); ?>;
						background-position: <?php echo esc_attr( $gl_header_bg_properties['position']); ?>;
						background-repeat: <?php echo esc_attr( $gl_header_bg_properties['repeat']); ?>;
						background-attachment: <?php echo esc_attr( $gl_header_bg_properties['attachment']); ?>;">
				   <div class="gl-title-txt">
					   <h1 class="greenland-title" ><?php echo wp_title(''); ?></h1>
					   <p class="greenland-subtitle"><?php echo esc_attr($gl_subtitle); ?></p>
				   </div>

			   </div>
	      <?php endif; ?>
	   <?php endif; ?>
	   <?php if(empty( $gl_subtitle_meta )||!$gl_header_bg_switch): ?>
		   <div class="tx-h2-min-height">
		   </div>

	   <?php endif; ?>
	   <div id="content" class="site-content">
