<?php
/**
 * Gland Helper function
 */


if ( ! function_exists( 'gland_cart' ) ) {
	add_action('gland_cart', function(){
		if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			$count = WC()->cart->cart_contents_count;
			$total =  WC()->cart->get_cart_total();
			if(!isset($total)){
				$total =0;
			}
			?>
			<div class="gl-cart">
				<div class="cart-total">
					<span><?php echo esc_html__('Cart -', 'greenland') ?></span>
					<?php echo wp_kses($total, wp_kses_allowed_html('post')); ?>
				</div>
				<div class="cart-contents">
					<span class="product-icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i> </span>
					<span class="product-count">  <?php echo esc_html($count) > 0 ? esc_html($count) : 0; ?>  </span>
				</div>
			</div>

			<?php
		}
	});
}

/*gland car 2*/
if ( ! function_exists( 'gland_cart2' ) ) {
	add_action('gland_cart2', function(){
		if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			$count = WC()->cart->cart_contents_count;
			$total = WC()->cart->get_cart_total();
			if ( ! isset( $total ) ) {
				$total = 0;
			}
			?>
			<div class="cart-contents">
				<span class="product-icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i> </span>
			</div>
			</div>
			<?php
		}
	});
}


/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
if ( ! function_exists( 'greenland_header_add_to_cart_fragment' ) ) {
	function greenland_header_add_to_cart_fragment( $fragments ) {
		ob_start();
		$count = WC()->cart->cart_contents_count;
		$total =  WC()->cart->get_cart_total();
		if(!isset($total)){
			$total =0;
		}
		?>
		<div class="gl-cart">
			<div class="cart-total">
				<span><?php esc_html_e('Cart -', 'greenland') ?></span><?php echo wp_kses($total, wp_kses_allowed_html('post')); ?>
			</div>
			<div class="cart-contents">
				<span class="product-icon"><i class="fa fa-shopping-cart" aria-hidden="true"></i> </span>
				<span class="product-count">  <?php echo esc_html($count) > 0 ? esc_html($count) : 0; ?>  </span>
			</div>
		</div>
		<?php
		$fragments['div.gl-cart'] = ob_get_clean();
		return $fragments;
	}
	add_filter( 'woocommerce_add_to_cart_fragments', 'greenland_header_add_to_cart_fragment' );
}




//core plugin
if ( ! function_exists( 'greenland_trim_title' ) ) {
	function greenland_trim_title($words){
		$excerpt = wp_trim_words(get_the_title(), $num_words = $words, $more = '');
		echo esc_html($excerpt);
	}
}

//excerpt more
if ( ! function_exists( 'greenland_new_excerpt_more' ) ) {
	function greenland_new_excerpt_more( $more ) {
		return esc_html_e('...', 'greenland');
	}
	add_filter('excerpt_more', 'greenland_new_excerpt_more');
}


/*
 * Custom Excerpt Length
 * */
if ( ! function_exists( 'greenland_custom_excerpt_length' ) ) {
	function greenland_custom_excerpt_length( $length ) {
		return 45;
	}
	add_filter( 'excerpt_length', 'greenland_custom_excerpt_length', 999 );
}





/**
 * Pagination
 */
if ( ! function_exists( 'greenland_pagination()' ) ) {
	function greenland_pagination() {

		if( is_singular() )
			return;

		global $wp_query;

		/** Stop execution if there's only 1 page */
		if( $wp_query->max_num_pages <= 1 )
			return;

		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = intval( $wp_query->max_num_pages );

		/**	Add current page to the array */
		if ( $paged >= 1 )
			$links[] = $paged;

		/**	Add the pages around the current page to the array */
		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}

		if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}

		echo '<ul class="pagination">' . "\n";

		/**	Previous Post Link */
		if ( get_previous_posts_link() )
			printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

		/**	Link to first page, plus ellipses if necessary */
		if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="active"' : '';

			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

			if ( ! in_array( 2, $links ) )
				echo '<li><a href="#">--</a></li>';
		}

		/**	Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
		}

		/**	Link to last page, plus ellipses if necessary */
		if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) )
				echo '<li><a href="#">--</a></li>' . "\n";

			$class = $paged == $max ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
		}

		/**	Next Post Link */
		if ( get_next_posts_link() )
			printf( '<li>%s</li>' . "\n", get_next_posts_link() );

		echo '</ul>' . "\n";

	}
}

/*
 * Get all type of pages ID
 * */
if ( ! function_exists( 'greenland_get_all_pages_ID' ) ) {
	function greenland_get_all_pages_ID(){
		$page_id = '';
		$page_id = get_the_ID();
		if ( class_exists( 'WooCommerce' ) ){
			if(is_shop()){
				$page_id = get_option( 'woocommerce_shop_page_id' );
			}
			if(is_cart()){
				$page_id = get_option( 'woocommerce_shop_page_id' );
			}
			if(is_checkout()){
				$page_id = get_option( 'woocommerce_shop_page_id' );
			}
		}

		/* add if woocommerce plugin exit
		 *  rest of pages
		 * */
		if(is_home()){
			$page_id = get_option( 'page_for_posts'  );
		}

		return $page_id;

	}
}


if ( ! function_exists( 'greenland_woocommerce_header_add_to_cart_fragment' ) ) {
	add_filter( 'woocommerce_add_to_cart_fragments', 'greenland_woocommerce_header_add_to_cart_fragment' );
	function greenland_woocommerce_header_add_to_cart_fragment( $fragments ) {
		ob_start();
		?>
		<a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php esc_attr_e( 'View your shopping cart','greenland' ); ?>"><?php echo sprintf (_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'greenland'  ), WC()->cart->cart_contents_count ); ?> - <?php echo WC()->cart->get_cart_total(); ?></a>
		<?php
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}

if ( ! function_exists( 'prefix_ajax_add_foobar' ) ) {
	add_action( 'wp_ajax_add_foobar', 'prefix_ajax_add_foobar' );
	add_action( 'wp_ajax_nopriv_add_foobar', 'prefix_ajax_add_foobar' );

	function prefix_ajax_add_foobar() {
		$product_id  = intval( $_POST['product_id'] );
		$none = $_POST['nonce'];
		$quantity = $_POST['quantity'];
		if(wp_verify_nonce($none,'search-ajax')):
// add code the add the product to your cart

			// if no products in cart, add it
			WC()->cart->add_to_cart( $product_id );


			add_filter('add_to_cart_fragments', 'greenland_woocommerce_header_add_to_cart_fragment');

			function woocommerce_header_add_to_cart_fragment( $fragments ) {
				global $woocommerce;

				ob_start();

				?>
				<span class="cart-count"><?php echo esc_html($woocommerce->cart->cart_contents_count); ?></span>
				<?php

				$fragments['span.cart-count'] = ob_get_clean();

				return $fragments;

			}

		endif;
		die();
	}
}



/*Comments Form Fields  position change*/
if ( ! function_exists( 'greenland_wpb_move_comment_field_to_bottom' ) ) {
	function greenland_wpb_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}

	add_filter( 'comment_form_fields', 'greenland_wpb_move_comment_field_to_bottom' );
}

/* Add Placehoder in comment Form Field (Comment) */
if ( ! function_exists( 'greenland_comment_form' ) ) {
	add_filter( 'comment_form_defaults', 'greenland_comment_form' );

	function greenland_comment_form( $args ) {
		$args['comment_field'] = '<div class="form-group comment-form-comment">

           <textarea class="form-control" id="comment" name="comment" placeholder="'._x( 'Your Comment', 'noun', 'greenland' ).'" cols="45" rows="8" aria-required="true"></textarea>
        </div>';
		$args['class_submit'] = 'tx-btn'; // since WP 4.1

		return $args;
	}
}


if ( ! function_exists( 'greenland_comment_form_fields' ) ) {
	add_filter( 'comment_form_default_fields', 'greenland_comment_form_fields' );

	function greenland_comment_form_fields( $fields ) {
		$commenter = wp_get_current_commenter();

		$req      = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		$html5    = current_theme_supports( 'html5', 'comment-form' ) ? 1 : 0;

		$fields   =  array(
			'author' => '<div class="row"><div class="col-md-12 form-group comment-form-author">' .
			            '<input class="form-control" placeholder ="'.esc_html__( "Name", 'greenland' ) . ( $req ? "*" : "" ) . '" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
			'email'  => '<div class="col-md-12 form-group comment-form-email">'.
			            '<input class="form-control" placeholder ="'.esc_html__( "Email", 'greenland' ) . ( $req ? "*" : "" ) . '" id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
			'url'    => '<div class="col-md-12 form-group comment-form-url">' .
			            '<input class="form-control" placeholder ="'.esc_html__( "URL", 'greenland' ) . '" id="url" name="url" ' . ( $html5 ? 'type="url"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></div></div>'
		);

		return $fields;
	}
}


//autor bio

if ( ! function_exists( 'greenland_author_info_box' ) ) {
	function greenland_author_info_box( $content ) {

		global $post;

// Detect if it is a single post with a post author
		if ( is_single() && isset( $post->post_author ) && get_post_type() == 'post' ) {

// Get author's display name
			$display_name = get_the_author_meta( 'display_name', $post->post_author );

// If display name is not available then use nickname as display name
			if ( empty( $display_name ) )
				$display_name = get_the_author_meta( 'nickname', $post->post_author );

// Get author's biographical information or description
			$user_description = get_the_author_meta( 'user_description', $post->post_author );

// Get author's website URL
			$user_website = get_the_author_meta('url', $post->post_author);

// Get link to the author archive page
			$user_posts = get_author_posts_url( get_the_author_meta( 'ID' , $post->post_author));

			if ( ! empty( $display_name ) )

				$author_details = '<p class="author_name"> '.esc_html__('About ','greenland') . $display_name . '</p>';

			if ( ! empty( $user_description ) )
// Author avatar and bio

				$author_details .= '<p class="author_details">' . get_avatar( get_the_author_meta('user_email') , 90 ) . nl2br( $user_description ). '</p>';

			$author_details .= '<p class="author_links"><a href="'. esc_url($user_posts) .'">'. esc_html__('View all posts by ','greenland') . esc_html($display_name) . '</a>';

// Check if author has a website in their profile
			if ( ! empty( $user_website ) ) {

// Display author website link
				$author_details .= ' | <a href="' . esc_url($user_website) .'" target="_blank" rel="nofollow">Website</a></p>';

			} else {
// if there is no author website then just close the paragraph
				$author_details .= '</p>';
			}

// Pass all this info to post content
			$content = $content . '<footer class="author_bio_section" >' . $author_details . '</footer>';
		}
		echo wp_kses_post($content);
	}

}
function greenland_author_content_hook(){
	echo do_action('greenland_author_content');
}
// Add our function to the post content filter
add_action( 'greenland_author_content', 'greenland_author_info_box' );
// Allow HTML in author bio section
remove_filter('pre_user_description', 'wp_filter_kses');




// Woocommerce css remove

// Remove each style one by oneafe
if ( ! function_exists( 'greenland_dequeue_styles' ) ) {
	add_filter( 'woocommerce_enqueue_styles', 'greenland_dequeue_styles' );
	function greenland_dequeue_styles( $enqueue_styles ) {
		unset( $enqueue_styles['woocommerce-general'] );	// Remove the gloss
		//unset( $enqueue_styles['woocommerce-layout'] );		// Remove the layout
		return $enqueue_styles;
	}
}

//custom nav
/**
 * Custom post navigation
 */
if ( ! function_exists( 'greenland_custom_nav' ) ) {
	function greenland_custom_nav(){
		$navigation = '';
		$previous   = get_previous_post_link( '<div class="nav-previous pull-left post-nav">%link</div>', '%title', true );
		$next       = get_next_post_link( '<div class="nav-next pull-right post-nav">%link</div>', '%title', true );

		// Only add markup if there's somewhere to navigate to.
		if ( $previous || $next ) {
			$navigation = _navigation_markup( $previous . $next, 'post-navigation' );
		}

		echo wp_kses($navigation, wp_kses_allowed_html('post'));
	}
}



/**
 * function greenland_title()
 * @param $words optional
 * how many words you want to display
 * in the title
 */
if(! function_exists('greenland_title')){
	function greenland_title($words){
		$excerpt = wp_trim_words(get_the_title(), $num_words = $words, $more = '');
		echo esc_html($excerpt);
	}
}


/**
 * Get option value
 * check greenland_get_option value
 */
if(!function_exists('greenland_get_option')) {
	function greenland_get_option($option) {
		if (!class_exists('CSFramework')) {
			return false;
		}
		return cs_get_option($option);
	}
}

/**
 * greenland_array_get();
 * get the array from
 * the return array
 */
if(!function_exists('greenland_array_get')) {
    function greenland_array_get($array, $key, $default=null){
        if(!is_array($array)) return $default;
        return array_key_exists($key, $array) ? $array[$key] : $default;
    }
}