<?php

/**
 * New Coded
 * Register Sidebar
 */
function greenland_sidebar_init() {
    /**
     * Default sidebar
     */
    register_sidebar( array(
        'name'          => esc_html__( 'Default Sidebar', 'greenland' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__('Blog page Sidebar', 'greenland'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    /**
     * Service Sidebar
     */
    register_sidebar( array(
        'name'          => esc_html__( 'Service', 'greenland' ),
        'id'            => 'service',
        'description'   => esc_html__('Service page Sidebar', 'greenland'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
    /**
     * Footer 1 sidebar
     */
    register_sidebar( array(
        'name'          => esc_html__( 'Footer-1', 'greenland' ),
        'id'            => 'footer-1',
        'description'   => esc_html__('Footer 1 sidebar', 'greenland'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    /**
     * Footer 2 Sidebar
     */
    register_sidebar( array(
        'name'          => esc_html__( 'Footer-2', 'greenland' ),
        'id'            => 'footer-2',
        'description'   => esc_html__('Footer 2 sidebar', 'greenland'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    /**
     * Footer 3 sidebar
     */
    register_sidebar( array(
        'name'          => esc_html__( 'Footer-3', 'greenland' ),
        'id'            => 'footer-3',
        'description'   => esc_html__('Footer 3 sidebar', 'greenland'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    /**
     * Footer 4 Sidebar
     */
    register_sidebar( array(
        'name'          => esc_html__( 'Footer-4', 'greenland' ),
        'id'            => 'footer-4',
        'description'   => esc_html__('Footer 4 sidebar', 'greenland'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    /**
     * Footer Bottom Sidebar
     */
    register_sidebar( array(
        'name'          => esc_html__( 'Footer Bottom', 'greenland' ),
        'id'            => 'footer-bottom',
        'description'   => esc_html__('Footer Bottom Menu', 'greenland'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'greenland_sidebar_init' );


