<?php
/*
 * Custom Typography For Greenlan
 *
 */

function greenland_typo_framework_options( $options ) {


	$options[] = array(
		'name'   => esc_html__( 'tx_typo', 'greenland' ),
		'title'  => esc_html__( 'Typography', 'greenland' ),
		'icon'   => 'fa fa-text-height',
		'fields' => array(

			/**
             * Enable custom typography
             */
            array(
                'id'        => 'greenland_custom_color',
                'type'      => 'switcher',
                'title'     => esc_html__('Enable custom color', 'greenland'),
                'default'   => false,
            ),

			/**
             * Enable body font color
             */
            array(
                'id'      => 'body_font_color',
                'type'    => 'color_picker',
                'title'   => esc_html__('Body Font color', 'greenland'),
                'default' => '#444',
                'rgba'    => true,
                'dependency'    => array('greenland_custom_color', '==', 'true'),
            ),

            /**
             * Enable body font color
             */
            array(
                'id'      => 'heading_font_color',
                'type'    => 'color_picker',
                'title'   => esc_html__('Heading Font color', 'greenland'),
                'default' => '#6ab42f',
                'rgba'    => true,
                'dependency'    => array('greenland_custom_color', '==', 'true'),
            ),


			/**
			 * Tigle Class font  Font Select
			 */

			array(
				'id'      => 'tx_title_font',
				'type'    => 'typography',
				'title'   => esc_html__( 'All Title font and style.', 'greenland' ),
				'desc'    => esc_html__( 'Use (.greenland-title) class to get this font', 'greenland' ),
				'default' => array(
					'family'  => 'Love Ya Like A Sister',
					'font'    => 'google', // this is helper for output ( google, websafe, custom )
					'variant' => '600',
				),

			),

			/**
			 * Blockqute  Font Select
			 */

			array(
				'id'      => 'tx_blockquote_font',
				'type'    => 'typography',
				'title'   => esc_html__( 'Select BlockQuote font and style.', 'greenland' ),
				'default' => array(
					'family'  => 'Poppins',
					'font'    => 'google', // this is helper for output ( google, websafe, custom )
					'variant' => '300',
				),

			),

			/**
			 * blockquote  Font Size
			 */

			array(
				'id'      => 'tx_blockquote_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'Blockquote - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Blockquote font size', 'greenland' ),
				'default' => '16',

			),

			/*line height*/
			array(
				'id'      => 'tx_blockquote_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'Blockquote Line Height', 'greenland' ),
				'default' => '24',
			),


			/**
			 * Header Font Select
			 */

			array(
				'id'      => 'tx_header_font',
				'type'    => 'typography',
				'title'   => esc_html__( 'Select Header (h1 to h6) font and style.', 'greenland' ),
				'default' => array(
					'family'  => 'Poppins',
					'font'    => 'google', // this is helper for output ( google, websafe, custom )
					'variant' => '300',
				),

			),


			/**
			 * Header 1 Font Size
			 */

			array(
				'id'      => 'tx_h1_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'H1 - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Header 1 (h1) font size.', 'greenland' ),
				'default' => '48',

			),

			/*line height*/
			array(
				'id'      => 'tx_h1_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'H1 Line Height', 'greenland' ),
				'default' => '56',
			),


			/**
			 * Header 2 Font Size
			 */

			array(
				'id'      => 'tx_h2_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'H2 - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Header 2 (h2) font size.', 'greenland' ),
				'default' => '28',

			),

			/*line height*/
			array(
				'id'      => 'tx_h2_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'H2 Line Height', 'greenland' ),
				'default' => '48',
			),

			/**
			 * Header 3 Font Size
			 */

			array(
				'id'      => 'tx_h3_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'H3 - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Header 3 (h3) font size.', 'greenland' ),
				'default' => '22',

			),

			/*line height*/
			array(
				'id'      => 'tx_h3_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'H3 Line Height', 'greenland' ),
				'default' => '33',
			),

			/**
			 * Header 4 Font Size
			 */

			array(
				'id'      => 'tx_h4_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'H4 - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Header 4 (h4) font size.', 'greenland' ),
				'default' => '28',

			),

			/*line height*/
			array(
				'id'      => 'tx_h4_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'H4 Line Height', 'greenland' ),
				'default' => '36',
			),

			/**
			 * Header 5 Font Size
			 */

			array(
				'id'      => 'tx_h5_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'H5 - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Header 5 (h5) font size.', 'greenland' ),
				'default' => '24',

			),

			/*line height*/
			array(
				'id'      => 'tx_h5_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'H5 Line Height', 'greenland' ),
				'default' => '30',
			),

			/**
			 * Header 6 Font Size
			 */

			array(
				'id'      => 'tx_h6_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'H6 - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Header 6 (h6) font size.', 'greenland' ),
				'default' => '20',

			),

			/*line height*/
			array(
				'id'      => 'tx_h6_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'H6 Line Height', 'greenland' ),
				'default' => '26',
			),

			/**
			 * Body (a, p ,span) Font Select
			 */

			array(
				'id'      => 'tx_paragraph_font',
				'type'    => 'typography',
				'title'   => esc_html__( 'Select Body font and style.', 'greenland' ),
				'desc'    => esc_html__( 'Body font (a , p , span ...)', 'greenland' ),
				'default' => array(
					'family'  => 'Poppins',
					'font'    => 'google', // this is helper for output ( google, websafe, custom )
					'variant' => '300',
				),

			),


			/**
			 * Paragraph Font Size
			 */

			array(
				'id'      => 'tx_p_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'Body - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Body font (a , p , span ...)', 'greenland' ),
				'default' => '14',

			),


			/*line height*/
			array(
				'id'      => 'tx_p_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'Body Font Line Height', 'greenland' ),
				'default' => '27',
			),


			/**
			 * Widget Heading font style
			 *
			 */
			array(
				'id'      => 'tx_widget_font',
				'type'    => 'typography',
				'title'   => esc_html__( 'Select Widget font and style.', 'greenland' ),
				'default' => array(
					'family'  => 'Poppins',
					'font'    => 'google', // this is helper for output ( google, websafe, custom )
					'variant' => '300',
				),

			),

			/**
			 * Widget Font Size
			 */

			array(
				'id'      => 'tx_widget_font_size',
				'type'    => 'number',
				'title'   => esc_html__( 'Widget - Font Size', 'greenland' ),
				'desc'    => esc_html__( 'Widget font size.', 'greenland' ),
				'default' => '20',

			),

			/*line height*/
			array(
				'id'      => 'tx_widget_line_height',
				'type'    => 'number',
				'title'   => esc_html__( 'Widget font Line Height', 'greenland' ),
				'default' => '26',
			),


		)
	);

	return $options;

}

add_filter( 'cs_framework_options', 'greenland_typo_framework_options' );