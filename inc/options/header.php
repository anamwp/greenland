<?php

function greenland_header_framework_options( $options ) {

    $options[]    = array(
        'name'      => 'tx_header',
        'title'     => esc_html__( 'Header Settings', 'greenland' ),
        'icon'      => 'fa fa-heart',
        'fields'    => array(

            /**
             * Header Variation Select
             */

            array(
                'id'           => 'tx_header_select',
                'type'         => 'image_select',
                'title'        => esc_html__('Select Header Variation', 'greenland'),
                'options'      => array(
                    '1'    => get_template_directory_uri() . '/inc/options/images/header-1.png',
                    '2'    => get_template_directory_uri() . '/inc/options/images/header-2.png'

                ),
                'default'      => '1'
            ),

            /*
             * Header 1 extra button
             * */

            array(
                'id'           => 'tx_extra_button',
                'type'         => 'switcher',
                'title'        => esc_html__('Extra Button', 'greenland'),
                'desc'         => esc_html__('Enable Extra Button.', 'greenland'),
                'default'      => '1'
            ),

            /**
             * Top Bar address
             */

            array(
                'id'        => 'tx_extra_button_properties',
                'type'      => 'fieldset',
                'title'     => esc_html__('Extra Button Properties', 'greenland'),
                'fields'    => array(

                    array(
                        'id'    => 'tx_1st_button_url',
                        'type'  => 'text',
                        'title' => esc_html__('URL', 'greenland'),
                        'desc'  => esc_html__('First Button Link.', 'greenland'),
                    ),

                    array(
                        'id'    => 'tx_1st_button_icon',
                        'type'  => 'text',
                        'title' => esc_html__('icon', 'greenland'),
                        'desc'  => esc_html__('Add your Fontawesome icon  Ex:fa-fa-phone.', 'greenland'),
                    ),

                    array(
                        'id'    => 'tx_1st_button_text',
                        'type'  => 'text',
                        'title' => esc_html__('Text', 'greenland'),
                        'desc'  => esc_html__('Button Text', 'greenland'),
                    ),

                    array(
                        'id'    => 'tx_2nd_button_url',
                        'type'  => 'text',
                        'title' => esc_html__('URL', 'greenland'),
                        'desc'  => esc_html__('Second Button Link.', 'greenland'),
                    ),

                    array(
                        'id'    => 'tx_2nd_button_icon',
                        'type'  => 'text',
                        'title' => esc_html__('icon', 'greenland'),
                        'desc'  => esc_html__('Add your Fontawesome icon  Ex:fa-fa-phone.', 'greenland'),
                    ),

                    array(
                        'id'    => 'tx_2nd_button_text',
                        'type'  => 'text',
                        'title' => esc_html__('Text', 'greenland'),
                        'desc'  => esc_html__('Button Text', 'greenland'),
                    ),

                ),
                'dependency'   => array( 'tx_extra_button', '==', 'true' ),
            ),


            /**
             * Top Bar
             */

            array(
                'id'           => 'tx_top_bar',
                'type'         => 'switcher',
                'title'        => esc_html__('Top Bar', 'greenland'),
                'desc'         => esc_html__('Enable top bar.', 'greenland'),

            ),


            /**
             * Top Bar address
             */

            array(
                'id'        => 'tx_top_address',
                'type'      => 'fieldset',
                'title'     => esc_html__('Address', 'greenland'),
                'fields'    => array(

                    array(
                        'id'    => 'tx_top_address',
                        'type'  => 'text',
                        'title' => esc_html__('Address', 'greenland'),
                        'desc'  => esc_html__('Enter your Address.', 'greenland'),
                    ),

                    array(
                        'id'    => 'tx_top_cell_no',
                        'type'  => 'text',
                        'title' => esc_html__('Contact Number', 'greenland'),
                        'desc'  => esc_html__('Enter your Contact Number.', 'greenland')
                    ),
                    array(
                        'id'    => 'tx_top_email',
                        'type'  => 'text',
                        'title' => esc_html__('Email Address', 'greenland'),
                        'desc'  => esc_html__('Enter your Email address.', 'greenland')
                    ),



                ),
                'dependency'   => array( 'tx_top_bar', '==', 'true' ),
            ),




            // ------------------------------------

            /**
             * Social Icon Link
             */

            array(
                'id'           => 'tx_social_icons',
                'type'         => 'switcher',
                'title'        => esc_html__('Social Icons', 'greenland'),
                'desc'         => esc_html__('Enable social icons.', 'greenland'),
            ),

            array(
                'id'        => 'tx_top_social',
                'type'      => 'fieldset',
                'title'     => esc_html__('Social Link', 'greenland'),
                'fields'    => array(

                    array(
                        'id'    => 'tx_top_social_fb',
                        'type'  => 'text',
                        'title' => esc_html__('Facebook', 'greenland'),
                        'desc'  => esc_html__('Enter your facebook link.', 'greenland'),
                    ),

                    array(
                        'id'    => 'tx_top_social_tw',
                        'type'  => 'text',
                        'title' => esc_html__('Twitter', 'greenland'),
                        'desc'  => esc_html__('Enter your twitter link.', 'greenland')
                    ),
                    array(
                        'id'    => 'tx_top_social_yt',
                        'type'  => 'text',
                        'title' => esc_html__('Youtube', 'greenland'),
                        'desc'  => esc_html__('Enter your youtube link.', 'greenland')
                    ),
                    array(
                        'id'    => 'tx_top_social_ln',
                        'type'  => 'text',
                        'title' => esc_html__('LinkedIn', 'greenland'),
                        'desc'  => esc_html__('Enter your linkedin link.', 'greenland')
                    ),
                    array(
                        'id'    => 'tx_top_social_sk',
                        'type'  => 'text',
                        'title' => esc_html__('Skype', 'greenland'),
                        'desc'  => esc_html__('Enter your Skype link.', 'greenland')
                    ),


                ),
                'dependency'   => array( 'tx_social_icons', '==', 'true' ),
            ),
            // ------------------------------------

        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'greenland_header_framework_options' );