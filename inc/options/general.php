<?php

function greenland_general_framework_options( $options ) {

    $options      = array(); // remove old options

    $options[]    = array(
        'name'      => 'tx_general',
        'title'     => esc_html__('General Settings', 'greenland'),
        'icon'      => 'fa fa-cogs',
        'fields'    => array(

            /**
             * Logo Upload
             */

            array(
                'id'    => 'tx_logo_header_1',
                'type'  => 'image',
                'title' => esc_html__('Header 1 Logo', 'greenland'),
                'desc'  => esc_html__('Upload a site logo for your branding.', 'greenland'),
            ),

            array(
                'id'    => 'tx_logo_header_2',
                'type'  => 'image',
                'title' => esc_html__('Header 2 Logo', 'greenland'),
                'desc'  => esc_html__('Upload a site logo for your branding.', 'greenland'),
            ),
            /**
             * Enable Boxed layout
             */
            array(
                'id'        => 'boxed_width',
                'type'      => 'switcher',
                'title'     => esc_html__('Enable Boxed Width', 'greenland'),
                'default'   => false,
            ),
            array(
                'id'           => 'background-image',
                'type'         => 'background',
                'title'        => esc_html__('Background Image', 'greenland'),
                'default'      => array(
                    'image'      => '',
                    'repeat'     => '',
                    'position'   => '',
                    'attachment' => '',
                    'size'       => '',
                    'color'      => '',
                ),
                'dependency'   => array( 'boxed_width', '==', 'true' ),
            ),

        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'greenland_general_framework_options' );