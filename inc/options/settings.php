<?php

/**
 * Codestar Framework Settings
 */

function greenland_option_settings( $settings ){
    $settings = array();
    $settings           = array(
	  'menu_title'      => esc_html__('Greenland','greenland'),
	  'menu_type'       => 'menu', // menu, submenu, options, theme, etc.
	  'menu_slug'       => 'greenland-options',
	  'ajax_save'       => true,
	  'show_reset_all'  => false,
	  'framework_title' => wp_kses('Greenland Options <small>by ThemeXpert</small>',array('small'=>array())),
	);
	
    return $settings;
}



add_filter('cs_framework_settings', 'greenland_option_settings');



