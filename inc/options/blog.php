<?php

function greenland_blog_framework_options( $options ) {


    $options[]    = array(
        'name'      => 'tx_blog',
        'title'     => esc_html__('Blog', 'greenland'),
        'icon'      => 'fa fa-adjust',
        'fields'    => array(

            /**
             * Blog Position
             */

            array(
                'id'        => 'tx_blog_position',
                'type'      => 'select',
                'title'     => esc_html__('Blog Single Blog Position', 'greenland'),
                'options'   => array(
                    'left'   => esc_html__( 'Left', 'greenland' ),
                    'right'   => esc_html__( 'Right', 'greenland' ),
                    'center'    => esc_html__( 'Center', 'greenland' ),
                ),
                'default'   => 'center',
                'desc'      => esc_html__('Select default blog text position.', 'greenland'),
            ),


        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'greenland_blog_framework_options' );