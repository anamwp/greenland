<?php
function greenland_footer_framework_options( $options ) {


	$options[] = array(
		'name'   => 'tx_footer',
		'title'  => esc_html__( 'Footer Options', 'greenland' ),
		'icon'   => 'fa fa-anchor',
		'fields' => array(


			/*
			 * Footer Type
			 * */

			array(
				'id'      => 'tx_footer_type',
				'type'    => 'select',
				'title'   => esc_html__( 'Select Your Footer Type', 'greenland' ),
				'options' => array(
					'widget'   => esc_html__( 'Widget', 'greenland' ),
					'nofooter' => esc_html__( 'NO Footer', 'greenland' ),
				),
				'default' => 'widget',
			),


			/**
			 * Footer Column Select
			 */

			array(
				'id'         => 'tx_footer_column',
				'type'       => 'image_select',
				'title'      => esc_html__( 'Footer Column', 'greenland' ),
				'desc'       => esc_html__( 'Select footer column number.', 'greenland' ),
				'options'    => array(
					'col_1'   => get_template_directory_uri() . '/inc/options/images/1col.jpg',
					'col_2_1' => get_template_directory_uri() . '/inc/options/images/2cols-3.jpg',
					'col_2_2' => get_template_directory_uri() . '/inc/options/images/2cols.jpg',
					'col_2_3' => get_template_directory_uri() . '/inc/options/images/2cols-2.jpg',
					'col_3'   => get_template_directory_uri() . '/inc/options/images/3cols.jpg',
					'col_3_3' => get_template_directory_uri() . '/inc/options/images/3cols-3.jpg',
					'col_3_2' => get_template_directory_uri() . '/inc/options/images/3cols-2.jpg',
					'col_4'   => get_template_directory_uri() . '/inc/options/images/4cols.jpg',
				),
				'default'    => 'col_3',
				'dependency' => array( 'tx_footer_type', '==', 'widget' ),

			),

			/**
			 * Custom Footer Background
			 */

			array(
				'id'           => 'tx_custom_footer_bg',
				'type'         => 'switcher',
				'title'        => esc_html__('Custom Background', 'greenland'),
				'desc'         => esc_html__('Enable custom footer background.', 'greenland'),
			),


			/**
			 * Footer Background Color
			 */

			array(
				'id'      => 'tx_custom_footer_bg_color',
				'type'    => 'color_picker',
				'title'   => esc_html__('Footer Background Color', 'greenland'),
				'rgba'    => true,
				'dependency'   => array( 'tx_custom_footer_bg', '==', 'true' ),
			),

			array(
				'id'      => 'tx_custom_footer_bottom_bg_color',
				'type'    => 'color_picker',
				'title'   => esc_html__('Footer Bottom Background Color', 'greenland'),
				'rgba'    => true,
				'dependency'   => array( 'tx_custom_footer_bg', '==', 'true' ),
			),

			/**
			 * Background Image
			 */

			array(
				'id'    => 'tx_custom_footer_bg_img',
				'type'  => 'background',
				'title' => esc_html__('Background Image', 'greenland'),
				'desc'  => esc_html__('Upload a background image for Footer.', 'greenland'),
				'dependency'   => array( 'tx_custom_footer_bg', '==', 'true' ),
			),

			/**
			 * Copyright Text
			 */

			array(
				'id'    => 'tx_footer_copy',
				'type'  => 'textarea',
				'title' => esc_html__( 'Copyright Text', 'greenland' ),
				'desc'  => esc_html__( 'Write footer copyright text here.', 'greenland' ),
			),

		)
	);
	// ------------------------------
	// Backup                       -
	// ------------------------------
	$options[]   = array(
		'name'     => 'backup_section',
		'title'    => esc_html__('Backup', 'greenland'),
		'icon'     => 'fa fa-shield',
		'fields'   => array(

			array(
				'type'    => 'notice',
				'class'   => 'warning',
				'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'greenland'),
			),

			array(
				'type'    => 'backup',
			),

		)
	);

	return $options;

}

add_filter( 'cs_framework_options', 'greenland_footer_framework_options' );