<?php

function greenland_sidebar_framework_options( $options ) {


    $options[]    = array(
        'name'      => 'tx_sidebar',
        'title'     => esc_html__('Sidebar', 'greenland'),
        'icon'      => 'fa fa-hand-scissors-o',
        'fields'    => array(

            /**
             * Sidebar Position
             */

            array(
                'id'        => 'tx_sidebar_position',
                'type'      => 'select',
                'title'     => esc_html__('Blog Single Sidebar Position', 'greenland'),
                'options'   => array(
                    'left'   => esc_html__( 'Left', 'greenland' ),
                    'right'   => esc_html__( 'Right', 'greenland' ),
                    'none'    => esc_html__( 'none', 'greenland' ),
                ),
                'default'   => 'right',
                'desc'      => esc_html__('Select default sidebar position.', 'greenland'),
            ),


        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'greenland_sidebar_framework_options' );