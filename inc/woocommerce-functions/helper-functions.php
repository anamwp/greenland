<?php
/**
 *All wocommerce hook functions
 */

/**
 * Woocommerce get product categories
 */
if ( class_exists( 'WooCommerce' ) ):
function greenland_shop_categories() {
	global $post;
	global $product;
	$parentid = $product->id;
	$terms    = wp_get_post_terms( $parentid, 'product_cat' );
	ob_start();
	?>
	<?php foreach ( $terms as $term ): ?>
		<a class="shop-cat" href="<?php echo esc_attr( get_term_link( $term ) ); ?>">
			<?php echo esc_html( $term->name ); ?>
		</a>
		<?php
	endforeach;
	$output = ob_get_clean();
	return $output;
}

/**
 * shop page ease product title, cat , price , rating
 */
add_action( 'woocommerce_after_shop_loop_item', 'greenland_shop_title', 5 );
function greenland_shop_title() {
	?>
	<div class="product-basic-info">

		<a href="<?php echo get_the_permalink(); ?>">
			<h4 class="product-title"><?php the_title(); ?></h4>
		</a>

		<h3 class="product-price"><?php woocommerce_template_loop_price(); ?></h3>
		<?php woocommerce_template_loop_rating(); ?>
	</div><!--end of basic info-->
	<?php
}


/**
 * single proudct page custom basic info
 */
add_action( 'woocommerce_single_product_summary', 'greenland_single_product_cat', 8 );
function greenland_single_product_cat() {
	echo greenland_shop_categories();
}

add_action( 'woocommerce_single_product_summary', 'greenland_yith_functions', 35 );
function greenland_yith_functions() {
	if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) :
		echo do_shortcode( '[yith_wcwl_add_to_wishlist icon="fa fa-heart fa-fw"]' );
	endif;
}

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

/**
 * WooCommerce Loop Product Thumbs
 **/
if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
	function woocommerce_template_loop_product_thumbnail() {
		echo woocommerce_get_product_thumbnail();
	}
}
/**
 * WooCommerce Product Thumbnail
 */
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
	function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0 ) {
		global $post, $woocommerce;
		$output = '<div class="shop-loop-image">';
		if ( has_post_thumbnail() ) {
			$feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
			$output .= '<img src="' . esc_url( $feat_image_url ) . '" alt="' . get_the_title() . '" />';
		} else {
			$output .= '<img src="' . esc_url( woocommerce_placeholder_img_src() ) . '" alt="Placeholder" width="' . esc_attr( $placeholder_width ) . '" height="' . esc_attr( $placeholder_height ) . '" />';
		}
		$output .= '</div>';
		return $output;
	}
}
endif;
