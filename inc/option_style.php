<?php


function greenland_custom_typo() {



	$h_primary_color   = greenland_get_option( 'tx_header_p_color' );
	$h_secondary_color = greenland_get_option( 'tx_header_s_color' );

	$main_menu_colors       = greenland_get_option( 'tx_main_menu_color' );
	$main_menu_color_reg    = $main_menu_colors['tx_main_menu_reg'];
	$main_menu_color_hover  = $main_menu_colors['tx_main_menu_hover'];
	$main_menu_color_active = $main_menu_colors['tx_main_menu_active'];

	$top_menu_colors       = greenland_get_option( 'tx_top_menu_color' );
	$top_menu_color_reg    = $top_menu_colors['tx_top_menu_reg'];
	$top_menu_color_hover  = $top_menu_colors['tx_top_menu_reg'];
	$top_menu_color_active = $top_menu_colors['tx_top_menu_reg'];

	$title_typo           = greenland_get_option( 'tx_title_font' );
	$header_typo           = greenland_get_option( 'tx_header_font' );
	$blockquote_typo       = greenland_get_option( 'tx_blockquote_font' );
	$paragraph_typo        = greenland_get_option( 'tx_paragraph_font' );
	$widget_typo           = greenland_get_option( 'tx_widget_font' );



	?>

	<style>
		.greenland-title{
			font-family: <?php echo esc_html($title_typo['family']);?>;
			font-weight: <?php echo esc_html($title_typo['variant']);?>;
		}
		h1, h2, h3, h4, h5, h6  {
			font-family: <?php echo esc_html($header_typo['family']);?>;
			font-weight: <?php echo esc_html($header_typo['variant']);?>;
		}

		blockquote p {

				font-family: <?php echo esc_html($blockquote_typo['family']);?>;
				font-weight: <?php echo esc_html($blockquote_typo['variant']);?>;
				font-size: <?php echo greenland_get_option('tx_blockquote_font_size');?>px;
				line-height: <?php echo greenland_get_option('tx_blockquote_line_height')?>px;
				/*letter-spacing:0.5px;*/


		}

		body, p{
			font-family: <?php echo esc_html($paragraph_typo['family']);?>;
			font-weight: <?php echo esc_html($paragraph_typo['variant']);?>;
			font-size: <?php echo greenland_get_option('tx_p_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_p_line_height')?>px;
			/*letter-spacing:0.5px;*/

		}
		p{
			margin-bottom:20px;
		}

		h1 {
			font-size: <?php echo greenland_get_option('tx_h1_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_h1_line_height')?>px;
		}

		h2 {
			font-size: <?php echo greenland_get_option('tx_h2_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_h2_line_height')?>px;
		}

		h3 {
			font-size: <?php echo greenland_get_option('tx_h3_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_h3_line_height')?>px;
		}

		h4 {
			font-size: <?php echo greenland_get_option('tx_h4_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_h4_line_height')?>px;
		}

		h5 {
			font-size: <?php echo greenland_get_option('tx_h5_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_h5_line_height')?>px;
		}

		h6 {
			font-size: <?php echo greenland_get_option('tx_h6_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_h6_line_height')?>px;
		}

		#footer h1, #footer h2, #footer h3, #footer h4, #footer h5, #footer h6, .widget-title {
			font-family: <?php echo esc_html($widget_typo['family']);?>;;
			font-weight: <?php echo esc_html($widget_typo['variant']);?>;
			font-size: <?php echo greenland_get_option('tx_widget_font_size'); ?>px;
			line-height: <?php echo greenland_get_option('tx_widget_line_height')?>px;
		}

	</style>


	<?php
	if ( greenland_get_option( 'tx_custom_footer_bg' ) ):
		$footer_bg_color = greenland_get_option( 'tx_custom_footer_bg_color' );
		$footer_bottom_bg  = greenland_get_option( 'tx_custom_footer_bottom_bg_color' );
		$footer_bg_img     = greenland_get_option( 'tx_custom_footer_bg_img' );
		?>

		<style>
			.site-footer {
				background: url(<?php echo esc_url($footer_bg_img['image']); ?>)
				<?php echo esc_html($footer_bg_img['color']) . $footer_bg_img['repeat'] . $footer_bg_img['position'] . $footer_bg_img['attachment']; ?>;
				background-size: <?php echo esc_html($footer_bg_img['size']); ?> ;
			}

			#footer {
				background-color: <?php echo esc_html($footer_bg_color); ?>;
			}
			<?php if ($footer_bg_img['image']): ?>
			.footer-bootom{
				border-top: 1px dashed #eee;
			}
			<?php endif; ?>
			#sub-footer {
				background-color: <?php echo esc_html($footer_bottom_bg); ?>;
			}

			@media (max-width: 767px) {
				.site-footer{
					background: #000;
					opacity:1;
				}
			}

		</style>

		<?php
	endif;


}


add_action( 'wp_head', 'greenland_custom_typo' );
